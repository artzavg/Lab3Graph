#ifndef VECTOR_H
#define VECTOR_H

#include <QObject>

class Vector
{
    friend class Matrix;
public:
    // Конструкторы
    Vector(double [3]);
    Vector(double = 0, double = 0, double = 0);

private:
    double vector[3];
};

#endif // VECTOR_H
