#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPainter>
#include <QPaintEvent>
#include <QDebug>
#include <QTimer>
#include "matrix.h"
#include <QPair>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void paintEvent(QPaintEvent *) override;  // событие отрисовки окна
    void resizeEvent(QResizeEvent *) override;

private:
    QPointF generateStarCenter (QPointF, double, double, double, double);
    QPolygonF generatePentagram(QPointF, double, Matrix, Matrix, Matrix);
    void timerEvent(QTimerEvent *) override; // событие истечения времени таймера

    Ui::MainWindow *ui;
    double rotateAngle; // угол поворота фигуры
    double starRotateAngle;
    double starScale;
    bool isFirstTimeGenerated[5] = { true, true, true, true, true };
    bool areStarsDecreasing = true;
    QPair<double, double> starCenters[15];  // todo
    Matrix reflectX = Matrix::reflectX(); // матрица отражения относительно оси Y
    QPolygonF generateStar(QPointF, double, double, double);
    QPair<double, double> generateXYShift();

    // угол, на котором последующая окружность отстает от предыдущей
    // 360 / 5 = 72
    const double otherCircleAngle = -::qDegreesToRadians(72);
    // угол, на котором находится вершина треугольника
    const double starAngle = -::qDegreesToRadians(36);
    // отступ от границ окна
    const int padding = 10;
};
#endif // MAINWINDOW_H
