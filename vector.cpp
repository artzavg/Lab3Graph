#include "vector.h"

Vector::Vector(double v[3]) {
    for(size_t i = 0; i < 3; i++) {
        vector[i] = v[i];
    }
}

Vector::Vector(double x, double y, double z) {
    vector[0] = x;
    vector[1] = y;
    vector[2] = z;
}
