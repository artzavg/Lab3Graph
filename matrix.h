#ifndef MATRIX_H
#define MATRIX_H

#include <QObject>
#include <QtMath>
#include <QDebug>
#include <QPoint>
#include "vector.h"

class Matrix
{
public:
    // Конструкторы
    Matrix(double matrix[3][3]);
    Matrix(double = 0, double = 0, double = 0,
           double = 0, double = 0, double = 0,
           double = 0, double = 0, double = 0);

    // Операции с матрицами
    Matrix operator*(const Matrix&);
    Vector operator*(const Vector&);
    QPointF operator*(const QPointF&);

    // Преобразования
    static Matrix rotate(double angle); // вращение
    static Matrix translate(double xDiff, double yDiff); // перенос
    static Matrix scale(double xScale, double yScale); // масштабирование
    static Matrix reflectX(); // отражение относительно оси Y
    static Matrix reflectY(); // отражение относительно оси X

private:
    double matrix[3][3]; // матрица
};

#endif // MATRIX_H
