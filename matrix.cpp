#include "matrix.h"

Matrix::Matrix(double a[3][3]) {
    for(size_t i = 0; i < 3; i++) {
        for(size_t j = 0; j < 3; j++) {
            matrix[i][j] = a[i][j];
        }
    }
}

Matrix::Matrix(double a11, double a12, double a13,
               double a21, double a22, double a23,
               double a31, double a32, double a33) {
    matrix[0][0] = a11;
    matrix[0][1] = a12;
    matrix[0][2] = a13;
    matrix[1][0] = a21;
    matrix[1][1] = a22;
    matrix[1][2] = a23;
    matrix[2][0] = a31;
    matrix[2][1] = a32;
    matrix[2][2] = a33;
}

Matrix Matrix::operator*(const Matrix &other) {
    Matrix result = Matrix();
    for(size_t i = 0; i < 3; i++) {
        for(size_t j = 0; j < 3; j++) {
            for(size_t k = 0; k < 3; k++) {
                result.matrix[i][j] += matrix[i][k] * other.matrix[k][j];
            }
        }
    }

    return result;
}

Vector Matrix::operator*(const Vector &other) {
    Vector result = Vector();
    for(size_t i = 0; i < 3; i++) {
        for(size_t j = 0; j < 3; j++) {
            result.vector[i] += matrix[i][j] * other.vector[j];
        }
    }

    return result;
}

QPointF Matrix::operator*(const QPointF &other) {
    Vector otherVector = *this * Vector(other.x(), other.y(), 1);
    return QPointF(otherVector.vector[0],
            otherVector.vector[1]);
}

Matrix Matrix::rotate(double angle) {
    double cosAngle = qCos(angle),
           sinAngle = qSin(angle);

    return Matrix(cosAngle, -sinAngle, 0,
                  sinAngle, cosAngle, 0,
                  0, 0, 1);
}

Matrix Matrix::translate(double xDiff, double yDiff) {
    return Matrix(1, 0, xDiff,
                  0, 1, yDiff,
                  0, 0, 1);
}

Matrix Matrix::scale(double xScale, double yScale) {
    return Matrix(xScale, 0, 0,
                  0, yScale, 0,
                  0, 0, 1);
}

Matrix Matrix::reflectX() {
    return Matrix(-1, 0, 0,
                  0, 1, 0,
                  0, 0, 1);
}

Matrix Matrix::reflectY() {
    return Matrix(1, 0, 0,
                  0, -1, 0,
                  0, 0, 1);
}

