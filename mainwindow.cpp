#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QRandomGenerator>
#include <QtMath>
#include <QException>
#include <exception>
#include <QPair>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    rotateAngle = 0;
    starRotateAngle = 0;
    starScale = 1;
    startTimer(30);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *) {
    QPainter p(this);

    int windowWidth = width(),  // ширина окна
        halfWindowWidth = windowWidth >> 1,  // половина ширины
        quarterWindowWidth = halfWindowWidth >> 1,  // четверть ширины
        windowHeight = height(),  // высота окна
        halfWindowHeight = windowHeight >> 1;  // половина высоты

    // радиус маленьких окружностей
    double r = quarterWindowWidth / 5;
    // радиус звезд в маленьких окружностях
    double pentagramRadius = r / 5;
    // расстояние от центра половины окна до центра маленькой кружности
    double d = quarterWindowWidth - this->padding - r;
    // расстояние от центра половины окна до угла пятиугольника
    double halfD = d / 2;

    // матрица переноса из центра окна в центр левой части
    Matrix translateLeft = Matrix::translate(-quarterWindowWidth, 0);
    // матрица переноса из центра левой части в центр окна
    Matrix translateRight = Matrix::translate(quarterWindowWidth, 0);

    // переносим начало координат в центр окна
    p.translate(halfWindowWidth, halfWindowHeight);

    // центр первой окружности (расположен в центре окна)
    QPointF circleCenter = QPointF(d, 0);
    // угол пятиугольника
    QPointF pentagonEdge = QPointF(halfD, 0);
    QPointF starCenter = QPointF(-quarterWindowWidth, 0);
    QPointF rightStarCenter = QPointF(quarterWindowWidth, 0);

    QPolygonF pentagon,  // пятиугольник
              rightPentagon,  // правый пятиугольник
              trianglesLine,
              rightTrianglesLine,
              star,
              rightStar;

    for(size_t i = 0; i < 5; i++) {
        // угол, задающий расположение текущей окружности
        double circleRotateAngle = i * this->otherCircleAngle + this->rotateAngle;
        // угол, задающий расположение вершины треугольника
        double triangleRotateAngle = (2*i + 1) * this->starAngle + this->rotateAngle;

        // матрица для поворота центра текущей окружности на данный угол
        Matrix circleRotateMatrix = Matrix::rotate(circleRotateAngle);
        // матрица для поворота центра текущей окружности на данный угол
        Matrix triangleRotateMatrix = Matrix::rotate(triangleRotateAngle);
        // todo: расставить комментарии

        // перенесенный в левую часть и повернутый центр текущей окружности
        QPointF newCircleCenter = translateLeft * circleRotateMatrix * circleCenter;

        for (size_t j = 0; j < 3; j++) {
            if (this->isFirstTimeGenerated[i]) {
                this->starCenters[i * 3 + j] = generateXYShift();
            }

            Matrix scaleMatrix = Matrix::scale(this->starScale, this->starScale);

            QPointF generatedStarCenter = generateStarCenter(newCircleCenter, r, pentagramRadius, this ->starCenters[i * 3 + j].first, this ->starCenters[i * 3 + j].second);
            QPointF rightGeneratedStarCenter = this->reflectX * generatedStarCenter;
            QPolygonF randomStar = generateStar(generatedStarCenter, pentagramRadius, pentagramRadius / 2, this->starRotateAngle);
            QPolygonF rightRandomStar = generateStar(rightGeneratedStarCenter, pentagramRadius, pentagramRadius / 2, -this->starRotateAngle);

            Matrix translateStar = Matrix::translate(generatedStarCenter.x(), generatedStarCenter.y());
            Matrix reverseTranslateStar = Matrix::translate(-generatedStarCenter.x(), -generatedStarCenter.y());
            Matrix rightTranslateStar = Matrix::translate(rightGeneratedStarCenter.x(), rightGeneratedStarCenter.y());
            Matrix rightReverseTranslateStar = Matrix::translate(-rightGeneratedStarCenter.x(), -rightGeneratedStarCenter.y());

            for (long long k = 0; k < 10; k++) {
                randomStar[k] = translateStar * scaleMatrix * reverseTranslateStar * randomStar[k];
                rightRandomStar[k] = rightTranslateStar * scaleMatrix * rightReverseTranslateStar * rightRandomStar[k];
            }

            p.drawPolygon(randomStar);
            p.drawPolygon(rightRandomStar);
        }

        if (this->isFirstTimeGenerated[i]) {
            this->isFirstTimeGenerated[i] = false;
        }

        // отраженный на правую часть центр окружности
        QPointF rightCircleCenter = this->reflectX * newCircleCenter;
        // перенесенный в левую часть и повернутый угол пятиугольника
        QPointF newPentagonEdge = translateLeft * circleRotateMatrix * pentagonEdge;
        // отраженный на правую угол пятиугольника
        QPointF rightPentagonEdge = this->reflectX * newPentagonEdge;
        //
        QPointF newTriangleEdge = translateLeft * triangleRotateMatrix * circleCenter;
        //
        QPointF rightTriangleEdge = this->reflectX * newTriangleEdge;

        pentagon.append(newPentagonEdge);
        rightPentagon.append(rightPentagonEdge);
        trianglesLine.append(newPentagonEdge);
        trianglesLine.append(newTriangleEdge);
        rightTrianglesLine.append(rightPentagonEdge);
        rightTrianglesLine.append(rightTriangleEdge);

        double starAngle = ::qDegreesToRadians(18) + this->rotateAngle;

        star = generateStar(starCenter, halfD, halfD / 2, starAngle);
        rightStar = generateStar(rightStarCenter, halfD, halfD / 2, -starAngle);

        p.drawEllipse(newCircleCenter, r, r);
        p.drawEllipse(rightCircleCenter, r, r);
    }

    p.drawPolygon(pentagon);
    p.drawPolygon(rightPentagon);
    p.drawPolygon(trianglesLine);
    p.drawPolygon(rightTrianglesLine);
    p.drawPolygon(star);
    p.drawPolygon(rightStar);

    this->rotateAngle += 0.0174533;  // поворачиваем рисунок на 1 градус
    this->starRotateAngle -= 0.0349066;

    if (this->areStarsDecreasing) {
        this->starScale -= 0.01;

        if (this->starScale < 0.3) {
            this->areStarsDecreasing = false;
        }
    } else {
        this->starScale += 0.01;

        if (this->starScale > 1.0) {
            this->areStarsDecreasing = true;
        }
    }
}

QPointF MainWindow::generateStarCenter (QPointF circleCenter, double circleRadius, double pentagramRadius, double x, double y) {
    if (circleRadius <= 1) {
        throw std::runtime_error("circleRadius is too little");
    }

    double newRadius = circleRadius - 2 * pentagramRadius;
    double newX = 2 * newRadius * x - newRadius;
    double newY = 2 * newRadius * y - newRadius;

    return QPointF(circleCenter.x() + newX, circleCenter.y() + newY);
}

QPair<double, double> MainWindow::generateXYShift() {
    double x = QRandomGenerator::global()->generateDouble();
    double y = QRandomGenerator::global()->bounded(qSqrt(1 - x * x));

    return QPair<double, double>(x, y);
}

QPolygonF MainWindow::generateStar(QPointF starCenter, double outerRadius, double innerRadius, double startAngle) {
    QPolygonF star;
    QPointF starPoint;
    bool isOuterRadius = true;
    double currentAngle = startAngle;

    for (size_t i = 0; i < 10; i++) {
        double sin = qSin(currentAngle),
               cos = qCos(currentAngle);

        if (isOuterRadius) {
            starPoint.setX(starCenter.x() + outerRadius * sin);
            starPoint.setY(starCenter.y() - outerRadius * cos);

            isOuterRadius = false;
        } else {
            starPoint.setX(starCenter.x() + innerRadius * sin);
            starPoint.setY(starCenter.y() - innerRadius * cos);

            isOuterRadius = true;
        }

        star.append(starPoint);
        currentAngle += this->starAngle;
    }

    return star;
}

void MainWindow::timerEvent(QTimerEvent *) {
    repaint();
}

void MainWindow::resizeEvent(QResizeEvent *event) {

}

